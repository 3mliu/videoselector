<!DOCTYPE html>

<html>
    <head>
        <title>Choose a video!</title>
        <meta charset="UTF-8">
        <meta name="viewport"  content="width=device-width, initial-scale=1.0">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="lazysizes.min.js" async=""></script>
        <script src="rotation.js"></script>
        <script src="watchvid.js"></script>
        <link rel="stylesheet" type="text/css" href="indexStyle.css">
        
    </head>
    <body>
         <div class='container'>
            <?php
                $username = "root";
                $password = "p@ssw0rd";
                $dsn = "130.211.236.143:3306";
                    
                $connection = mysqli_connect($dsn, $username, $password) or die("Connection failed");
                $config = basename("/config.txt");
                #$no_lines = count(file($config));
                $query = "SELECT * FROM video.videos WHERE url IS NOT NULL ORDER BY RAND() LIMIT 6";
                $result = mysqli_query($connection, $query);
                if (mysqli_num_rows($result) > 0) {
                    $i = 1;
                    while ($row = $result->fetch_assoc()) {
                        echo "<a href='#' class='vid" . (string) $i . "'>";
                        echo "<iframe id='vid" . (string) $i ."' width='280' height='208' src='https:///www.youtube.com/embed/"
                        . substr($row['url'], 17) . "?autohide=1&showinfo=0&controls=0" ."'>";
                        echo "</iframe>";
                        echo "</a>";
                        $i++;
                    }
                } else {
                    echo "No results found.";
                }
            ?>
        </div>
        <button id="select1">Select Video 1</button>
        <button id="select2">Select Video 2</button>
        <button id="select3">Select Video 3</button>
        <button id="select4">Select Video 4</button>
        <button id="select5">Select Video 5</button>
        <button id="select6">Select Video 6</button>
        
        
    </body>
</html>

