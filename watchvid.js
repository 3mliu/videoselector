$(document).ready(function() {
    var video = JSON.parse(localStorage.getItem("currVid"));
    var vidId = video.link.substr(79, 11);
    loadVid(vidId);
    
    $("#back-button").click(function() {
       window.location.href = "index.php"; 
    });
    
    $("#like-button").click(function() {
        if (video.liked) {
            video.liked = false;
        } else {
            if (video.disliked) {
                video.disliked = false;
            }
            video.liked = true;
        }
        console.log(video);
    });
    
    $("#dislike-button").click(function() {
        if (video.disliked) {
            video.disliked = false;
        } else {
            if (video.liked) {
                video.liked = false;
            }
            video.disliked = true;
        }
        console.log(video);
    });
    
    $("iframe").click(function() {
        console.log("Expanding");
        $("iframe").addClass("expanded");
    });

});

function loadVid(id) {
    $(".placeholder").append('<iframe id = "vid" width="560" height="315" src="https:///www.youtube.com/embed/' +
            id + '?rel=0&autohide=1&showinfo=0&controls=0"></iframe>');}